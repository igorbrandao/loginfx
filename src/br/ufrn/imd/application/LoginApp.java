package br.ufrn.imd.application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoginApp extends Application {

	private String WINDOW_STYLE;
	
	@Override
	public void start(Stage stage) throws Exception {
		try{
			// CSS
			WINDOW_STYLE = "-fx-background-color: linear-gradient("
					+ "from 0% 0% to 100% 100%, blue 0%, silver 100%);";
			
			AnchorPane pane = new AnchorPane();
			pane.setPrefSize(400, 300);

			// Create the components
			TextField txtLogin = new TextField();
			txtLogin.setPromptText("Digite aqui o seu login");

			PasswordField txtSenha = new PasswordField();
			txtSenha.setPromptText("Digite aqui sua senha");

			Button btnEntrar = new Button("Entrar");
			Button btnSair = new Button("Sair");

			// Add the elements into the screen
			pane.getChildren().addAll(txtLogin, txtSenha, btnEntrar, btnSair);

			// Display the screen
			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.show();
			
			// Define the position
			txtLogin.setLayoutX((pane.getWidth() - txtLogin.getWidth()) / 2);
			txtLogin.setLayoutY(50);

			txtSenha.setLayoutX((pane.getWidth() - txtSenha.getWidth()) / 2);
			txtSenha.setLayoutY(100);

			btnEntrar.setLayoutX((pane.getWidth() - btnEntrar.getWidth()) / 2);
			btnEntrar.setLayoutY(150);
			
			btnSair.setLayoutX((pane.getWidth() - btnSair.getWidth()) / 2);
			btnSair.setLayoutY(200);
			
			// Apply the CSS
			pane.setStyle(WINDOW_STYLE);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
